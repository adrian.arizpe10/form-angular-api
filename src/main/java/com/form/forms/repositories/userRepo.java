package com.form.forms.repositories;

import com.form.forms.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface userRepo extends JpaRepository<User, Long> {
    
}