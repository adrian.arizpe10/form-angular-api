package com.form.forms.controller;

import com.form.forms.model.User;
import com.form.forms.repositories.userRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private userRepo uRepo;

    @PostMapping("/create")
    public ResponseEntity<User> createPost(@RequestBody User user) {        
        return ResponseEntity.ok(uRepo.save(user));
    }
    
}